from random import randint

def crea_jeu ( ) :
        jeu = []
        oranges = [0 for i in range(4)]
        vertes = [i for i in range(1, 11)]
        violettes = [ i*10 for i in vertes if i%2 != 0]
        violettes = violettes * 2
        for i in vertes :
            jeu.append((1, i))
        for i in violettes :
            jeu.append((0, i))
        for i in oranges :
            jeu.append((2, i))
        return jeu

def distribue(jeu) :
        jeu1, jeu2 = [], []
        while len(jeu) != 0 :
            a = randint(0, len (jeu) - 1)
            jeu1.append(jeu[a])
            del jeu[a]
            b = randint(0, len(jeu) - 1 )
            jeu2.append(jeu[b])
            del jeu[b]
        return jeu1, jeu2

def pli (carte1, carte2) :
                """ carte1 et carte2 : tuples de la forme (Couleur, Valeur) """
                """ retourne un tuple : carte ayant remporte le pli """
                """ retourne None en cas d'egalite """
                if carte1[0] > carte2[0] : # On vérifie la couleur des cartes
                    return carte1
                elif carte1[0] < carte2[0] :
                    return carte2
                else :
                    if carte1[1] > carte2[1] : # si les couleurs sont identiques, on vérifie leur valeur
                        return carte1
                    elif carte1[1] < carte2[1] :
                        return carte2
                return None # si les cartes sont identiques on retourne None

def jouer_carte1(main) :
    """ main : la liste non vide de cartes représentant le jeu de la petite nièce 
    la fonction retourne la carte jouée """
   
    liste1 = [i[1] for i in main if i[0] == 1]
    liste2 = [i[1] for i in main if i[0] == 0]
    for i in range(len(main)) :
        if main[i][0] == 2 :
            carte = main[i]
            return carte
    for i in range(len(main)) :
        if main[i][0] == 0 and main[i][1] == min(liste2) :
            carte = main[i]
            return carte
    for i in range(len(main)) :
        if main[i][0] == 1 and main[i][1] == max(liste1) :
            carte = main[i]
            return carte

def jouer_carte2(main) :
            """ main est une liste de cartes que peut jouer le programme
            la fonction retourne la carte jouée"""
            liste1 = [i[1] for i in main if i[0] == 1]
            liste2 = [i[1] for i in main if i[0] == 0]
            for i in range(len(main)) :
                if main[i] == 2 :
                    carte = main[i]
                    return carte
            for i in range(len(main)) :
                if main[i][0] == 1 and main[i][1] == max(liste1) :
                    carte = main[i]
                    return carte
            for i in range(len(main)) :
                if main[i][0] == 0 and main[i][1] == min(liste2) :
                    carte = main[i]
                    return carte
            carte = main[0]
            return carte

def partie() :
            joueur1, joueur2 = distribue(crea_jeu())
            score1, score2 = 0, 0
            while len(joueur1) != 0 and len(joueur2) != 0 :
                carte_j1 = jouer_carte1(joueur1)
                joueur1.remove(carte_j1)
                carte_j2 = jouer_carte2(joueur2)
                joueur2.remove(carte_j2)
                gagnant = pli(carte_j1, carte_j2)
                if gagnant == carte_j1 :
                    score1 = score1 + carte_j1[1] + carte_j2[1]
                elif gagnant == carte_j2 :
                    score2 = score2 + carte_j1[1] + carte_j2[1] 
            if score1 > score2 :
                return 1
            elif score1 < score2 :
                return 2
            else :
                return 0

def parties(n) :
    g1, g2 = 0,0
    for i in range (n) :
        resultat = partie()
        if resultat == 1 :
            g1 += 1
        elif resultat == 2 :
            g2+=1
    return g1/n*100, g2/n*100
